package com.demo.validator;

import com.demo.validator.Language;

public class Rules {
	public static boolean isValid(String value, Language language) {
		String englishRegex = "\\A([\"']?[A-Z]{1}[a-z]*){1}([,:\"']?[ ]{1}([\"']?(?:(?:(?!\\bA\\b|\\bBut\\b|\\bFor\\b|\\\\bOr\\b|\\bNot\\b|\\bThe\\b|\\bAn\\b)([A-Z]{1}[a-z]*))|(?:\\ba\\b|\\bbut\\b|\\bfor\\b|\\bor\\b|\\bnot\\b|\\bthe\\b|\\ban\\b)){1}[,:\"']?[ ]{1})*[\"']?[A-Z]{1}[a-z]*[,:\"']?)?\\z";
		String russianRegex = "\\A([\"']?[А-Я]{1}[а-я]*){1}(?:([,:\"']? ([\"']?[а-я]+[,:\"']? )*([\"']?[а-я]+[\"']?){1})*|[\"']?)?\\z";
		
		boolean rus = false;
		boolean eng = false;
		if (language == Language.ANY) {
			rus = value.matches(russianRegex);
			if (!rus) {
				eng = value.matches(englishRegex);
				if (!eng) {
					return false;
				}
			}
			return true;
		}
		if (language == Language.RU) {
			rus = value.matches(russianRegex);
			return rus;
		} else if (language == Language.EN) {
			eng = value.matches(englishRegex);
			return eng;
		}
		return false;
	}
}