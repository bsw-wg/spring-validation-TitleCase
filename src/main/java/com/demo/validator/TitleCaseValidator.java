package com.demo.validator;

import com.demo.annotation.TitleCase;
import com.demo.validator.Rules;
import com.demo.validator.Language;

import java.util.Set;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class TitleCaseValidator implements ConstraintValidator<TitleCase, String>{
	private Language language;

	@Override
	public void initialize(TitleCase annotation){
		language = annotation.language();
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		return Rules.isValid(value, language);
	}
}