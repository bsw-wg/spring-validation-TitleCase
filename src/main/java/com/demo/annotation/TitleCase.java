package com.demo.annotation;

import com.demo.validator.TitleCaseValidator;
import com.demo.validator.Language;
import java.lang.annotation.*;
import javax.validation.Constraint;
import javax.validation.Payload;


@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Constraint(validatedBy = TitleCaseValidator.class)
public @interface TitleCase {
	Language language() default Language.ANY;

	String message() default "Bad values";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}