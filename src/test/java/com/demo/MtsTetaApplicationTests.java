package com.demo;

import com.demo.validator.Language;
import com.demo.validator.Rules;
import static org.junit.jupiter.api.Assertions.assertEquals;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest()
public class MtsTetaApplicationTests {
    @Test
    public void test1() throws Exception {
    	assertEquals(Rules.isValid("4fdsdf", Language.ANY), false);
    }

    @Test
    public void test2() throws Exception {
    	assertEquals(Rules.isValid("Fffff", Language.ANY), true);
    }
    

    @Test
    public void test3() throws Exception {
    	assertEquals(Rules.isValid("\"But But\"", Language.ANY), true);
    }

    @Test
    public void test4() throws Exception {
    	assertEquals(Rules.isValid("Test but Text Some", Language.ANY), true);
    }

    @Test
    public void test5() throws Exception {
    	assertEquals(Rules.isValid(" Text", Language.ANY), false);
    }

    @Test
    public void test6() throws Exception {
    	assertEquals(Rules.isValid("Text ", Language.ANY), false);
    }

    @Test
    public void test7() throws Exception {
    	assertEquals(Rules.isValid("text", Language.ANY), false);
    }

    @Test
    public void test8() throws Exception {
    	assertEquals(Rules.isValid("4fdsdf", Language.ANY), false);
    }

    @Test
    public void test9() throws Exception {
    	assertEquals(Rules.isValid("Double  space", Language.ANY), false);
    }

    @Test
    public void test10() throws Exception {
    	assertEquals(Rules.isValid("Абвгд", Language.ANY), true);
    }

    @Test
    public void test11() throws Exception {
    	assertEquals(Rules.isValid("Абвгд вавав", Language.ANY), true);
    }

    @Test
    public void test12() throws Exception {
    	assertEquals(Rules.isValid(" текст ", Language.ANY), false);
    }

    @Test
    public void test13() throws Exception {
    	assertEquals(Rules.isValid("Текст, маленький: \"очень\"", Language.ANY), true);
    }

    @Test
    public void test14() throws Exception {
        assertEquals(Rules.isValid("Сбольшойбуквы маленькой маленькой маленькой", Language.RU), true);
    }

    @Test
    public void test15() throws Exception {
        assertEquals(Rules.isValid("сбольшой нужно", Language.RU), false);
    }

    @Test
    public void test16() throws Exception {
        assertEquals(Rules.isValid("Этосбольшой Остальныесмаленькой Смаленькой", Language.RU), false);
    }


    @Test
    public void test17() throws Exception {
        assertEquals(Rules.isValid("БоЛьшая", Language.RU), false);
    }


    @Test
    public void test18() throws Exception {
        assertEquals(Rules.isValid("Толькорусский text", Language.RU), false);
    }


    @Test
    public void test19() throws Exception {
        assertEquals(Rules.isValid("But", Language.RU), false);
    }

    @Test
    public void test20() throws Exception {
        assertEquals(Rules.isValid(" Это, ошибка ", Language.RU), false);
    }

    @Test
    public void test21() throws Exception {
        assertEquals(Rules.isValid("Это, верно\"", Language.RU), true);
    }

    @Test
    public void test22() throws Exception {
        assertEquals(Rules.isValid("неправильно  два пробела", Language.RU), false);
    }

    @Test
    public void test23() throws Exception {
        assertEquals(Rules.isValid("Good Text Means No Errors", Language.EN), true);
    }

    @Test
    public void test24() throws Exception {
        assertEquals(Rules.isValid("Bad text", Language.EN), false);
    }
    
    @Test
    public void test25() throws Exception {
        assertEquals(Rules.isValid("Bad  Spaces", Language.EN), false);
    }
    
    @Test
    public void test26() throws Exception {
        assertEquals(Rules.isValid(" One More Bad Spaces Start", Language.EN), false);
    }   

    @Test
    public void test27() throws Exception {
        assertEquals(Rules.isValid("One More Bad Spaces End ", Language.EN), false);
    }   

    @Test
    public void test28() throws Exception {
        assertEquals(Rules.isValid(" One More Bad Spaces Start", Language.EN), false);
    }   

    @Test
    public void test29() throws Exception {
        assertEquals(Rules.isValid("Special Words Cheeeck a but for or not the an Correct", Language.EN), true);
    }   

    @Test
    public void test30() throws Exception {
        assertEquals(Rules.isValid("Special Case But", Language.EN), true);
    }   

    @Test
    public void test31() throws Exception {
        assertEquals(Rules.isValid("Special Case Two but", Language.EN), false);
    }   

    @Test
    public void test32() throws Exception {
        assertEquals(Rules.isValid("\"Punctuation\" Fantastic,", Language.EN), true);
    }   
}   
